Given("I am on the home page") do
  visit 'https://www.edgepipeline.com/' 
end

When("I start session") do
  find('a', :text=>"Sign In").click 
  fill_in 'username', with: 'intellek'
  fill_in 'password', with: '354035as'
  find(:css, "[name='button_action']").click
end

When("Condition Items Repair costs") do 
  #visit ('https://www.edgepipeline.com/components/vehicle/detail/daasmemphis/840933?btr=%2Fcomponents%2Freport%2Fpresale%2Fview%2Fdaasmemphis-all%2F2019%2F9%2F5%23vehicle_33409_81049')
  visit 'https://www.edgepipeline.com/components/vehicle/detail/daasmemphis/840845?btr=%2Fcomponents%2Freport%2Fpresale%2Fview%2Fdaasmemphis-all%2F2019%2F9%2F3%23vehicle_33409_81402'
  #visit 'https://www.edgepipeline.com/components/vehicle/detail/yourauctiontb/255305?btr=%2Fcomponents%2Freport%2Fpresale%2Fview%2Fyourauctiontb-all%2F2019%2F9%2F3%23vehicle_38042_49869'
  assert page.has_content? ('Total')
  array =[]
  trans = []
  totals = []
  sum = 0
  sum_f = 0
  @sum_2 = 0
  idx = 0

  for i in(0..27)
    span = all('.damage-row .field span')[i].text
    array.push(span)
  end

  array.each_slice(7) do |slice|
    total = slice[-1]
    slice.delete_at(-1)
    puts slice
    trans << slice

    for i in(slice)
      if i != "--"
        sum += i.delete("$").to_i
      end
    end
    assert sum.eql? (total.delete("$").to_i)
    sum = 0
  end

  for i in(0..6)
    span = all('.total-field')[i].text
    totals.push(span)
  end

  puts totals

  trans.transpose.each do |slices|
    for i in(slices)
      if i != "--"
        sum_f += i.delete("$").to_i
      end
    end
    puts sum_f
    assert sum_f.eql? (totals[idx].delete("$").to_i)
    idx += 1
    sum_f = 0
  end

  @total_ = totals[-1]
  totals.delete_at(-1)
  for i in(totals)
    if i != "--"
      @sum_2 += i.delete("$").to_i
    end
  end
  puts @sum_2

end

Then("All is OK") do
  assert @sum_2.eql? (@total_.delete("$").to_i)
end
